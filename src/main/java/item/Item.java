package item;

import application.ScoreCounter;

public abstract class Item {
    protected ScoreCounter scoreCounter = ScoreCounter.getInstance();

    public abstract void startRound(Items userChoice);

}
