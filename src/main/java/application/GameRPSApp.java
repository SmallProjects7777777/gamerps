package application;

public class GameRPSApp {

    public static void main(String[] args) {
        Competition competition = new Competition();

        competition.startGame();
        competition.endGame();
    }
}
