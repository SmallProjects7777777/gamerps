package application;

import lombok.Getter;

@Getter
public class ScoreCounter {
    private int scoreUser;
    private int scoreCpu;
    private static ScoreCounter instance;

    public static ScoreCounter getInstance() {
        if (instance == null) {
            instance = new ScoreCounter();
        }
        return instance;
    }

    private ScoreCounter() {}

    public void incrementUserPoints() {
        scoreUser++;
    }

    public void incrementCpuPoints() {
        scoreCpu++;
    }

}
