package item;

public class ItemScissors extends Item {
    @Override
    public void startRound(Items userChoice) {
        if(userChoice == Items.SCISSORS) {
            System.out.println("Mamy remis");
        } else if(userChoice == Items.PAPER) {
            System.out.println("Przegrałeś");
            scoreCounter.incrementCpuPoints();
        } else {
            System.out.println("Wygrałeś");
            scoreCounter.incrementUserPoints();
        }
    }
}
