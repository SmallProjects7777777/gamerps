package application;

import choice.CPU;
import choice.User;
import item.*;

public class Competition {
    User user = new User();
    CPU cpu = new CPU();
    ItemPaper itemPaper = new ItemPaper();
    ItemRock itemRock = new ItemRock();
    ItemScissors itemScissors = new ItemScissors();
    ScoreCounter scoreCounter = ScoreCounter.getInstance();
    Items userChoice;
    Items cpuChoice;


    public void startGame() {
        while(isLessThanLimit()) {
            userChoice = user.getAnswer();
            cpuChoice = cpu.getAnswer();
            showChoices(userChoice, cpuChoice);
            checkWhoWin(userChoice, cpuChoice);
            showActualScore();
        }

    }

    private boolean isLessThanLimit() {
        return scoreCounter.getScoreCpu() < 3 && scoreCounter.getScoreUser() < 3;
    }

    private void showActualScore() {
        System.out.println("TY " + scoreCounter.getScoreUser() + ":" +
                scoreCounter.getScoreCpu() + " PRZECIWNIK");
    }

    private void showChoices(Items userChoice, Items cpuChoice) {
        System.out.println("Twój wybór to: " + userChoice.getDescribe() +
                        "\nWybór przeciwnika to: " + cpuChoice.getDescribe());
    }

    private void checkWhoWin(Items userChoice, Items cpuChoice) {
        switch (cpuChoice){
            case ROCK -> itemRock.startRound(userChoice);
            case PAPER -> itemPaper.startRound(userChoice);
            case SCISSORS -> itemScissors.startRound(userChoice);
        }
    }


    public void endGame() {
        showFinishText(scoreCounter.getScoreUser(), scoreCounter.getScoreCpu());
        InputScanner.getScanner().close();
    }

    private void showFinishText(int scoreUser, int scoreCpu) {
        if(scoreUser > scoreCpu) {
            System.out.println("Brawo wygrałeś gre");
        } else {
            System.out.println("Niestety tym razem przegrałeś");
        }
    }
}
