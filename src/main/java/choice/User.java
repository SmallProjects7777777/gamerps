package choice;
import application.InputScanner;
import item.Items;

public class User extends Choice{

    @Override
    public Items getAnswer(){
        boolean isCorrect = false;
        int answer = 0;

        while(!isCorrect) {
            try{
                showStartQuestion();
                answer = Integer.parseInt(InputScanner.getScanner().nextLine());
                isCorrect = checkAnswerIsCorrect(answer);
                if(!isCorrect) {
                    showInfoForIncorrectAnswer();
                }
            } catch (NumberFormatException e) {
                System.out.println("Niepoprawny wybór, spróbuj jeszcze raz");
            }
        }
        return Items.getValues(answer);
    }

    private void showInfoForIncorrectAnswer() {
        System.out.println("Niepoprawny numer, wybierz numer 1, 2 lub 3");
    }

    private boolean checkAnswerIsCorrect(int answer) {
        return answer == 1 || answer == 2 || answer == 3;
    }


    private void showStartQuestion() {
        System.out.println("""
                Podaj twój wybór;
                1. Kamień
                2. Papier
                3. Nożyczki
                """);
    }
}
