package choice;

import item.Items;

public abstract class Choice {
    public abstract Items getAnswer();

}
