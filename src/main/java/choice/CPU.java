package choice;

import item.Items;

import java.util.Random;

public class CPU extends Choice{
    Random random = new Random();
    int itemsSize = Items.values().length;

    @Override
    public Items getAnswer() {

        int randomAnswer = random.nextInt(itemsSize) + 1;

        return Items.getValues(randomAnswer);
    }

}
