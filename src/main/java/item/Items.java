package item;

public enum Items {
    ROCK("Kamień"),
    PAPER("Papier"),
    SCISSORS("Nożyczki");

    final String describe;

    Items(String describe) {
        this.describe = describe;
    }

    public String getDescribe() {
        return describe;
    }



    private static final Items[] values = values();

    public static Items getValues(int choice) {
        return values[choice -1];
    }
}
