package item;

public class ItemRock extends Item {

    @Override
    public void startRound(Items userChoice) {
        if(userChoice == Items.ROCK) {
            System.out.println("Mamy remis");
        } else if(userChoice == Items.SCISSORS) {
            System.out.println("Przegrałeś");
            scoreCounter.incrementCpuPoints();
        } else {
            System.out.println("Wygrałeś");
            scoreCounter.incrementUserPoints();
        }
    }
}
